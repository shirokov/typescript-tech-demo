import HalfPlane, { Orientation } from "../src/half_plane";
import Rectangle from "../src/rectangle";
import { Geometry } from "../src/geometry";

describe('testing geometry', () => {
    // Rectangle intersections
    // -    one is NW of the other
    test('intersect Rectangle and Rectangel: one is NW of the other - no intersection', () => {
        const one: Rectangle = new Rectangle(0, 0, 5, 5);
        const other: Rectangle = new Rectangle(10, 10, 10, 10);
        expect(Geometry.intersect(one, other)).toBe(undefined);
    });
    test('intersect Rectangle and Rectangel: one is NW of the other - intersection', () => {
        const one: Rectangle = new Rectangle(5, 5, 10, 10);
        const other: Rectangle = new Rectangle(10, 10, 10, 10);
        expect(Geometry.intersect(one, other)).toStrictEqual(new Rectangle(10, 10, 5, 5));
    });
    test('intersect Rectangle and Rectangel: one is NW of the other - trivial case', () => {
        const one: Rectangle = new Rectangle(5, 5, 5, 5);
        const other: Rectangle = new Rectangle(10, 10, 10, 10);
        expect(Geometry.intersect(one, other)).toStrictEqual(new Rectangle(10, 10, 0, 0));
    });
    test('intersect Rectangle and Rectangel: one is NW of the other - one contains the other', () => {
        const one: Rectangle = new Rectangle(5, 5, 20, 20);
        const other: Rectangle = new Rectangle(10, 10, 10, 10);
        expect(Geometry.intersect(one, other)).toStrictEqual(other);
    });
    // -    one is N of the other
    test('intersect Rectangle and Rectangel: one is N of the other - no intersection', () => {
        const one: Rectangle = new Rectangle(25, 0, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toBe(undefined);
    });
    test('intersect Rectangle and Rectangel: one is N of the other - intersection', () => {
        const one: Rectangle = new Rectangle(25, 15, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toStrictEqual(new Rectangle(25, 20, 10, 5));
    });
    test('intersect Rectangle and Rectangel: one is N of the other - trivial case', () => {
        const one: Rectangle = new Rectangle(25, 10, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toStrictEqual(new Rectangle(25, 20, 10, 0));
    });
    test('intersect Rectangle and Rectangel: one is N of the other - one contains the other', () => {
        const one: Rectangle = new Rectangle(10, 10, 10, 20);
        const other: Rectangle = new Rectangle(10, 10, 10, 10);
        expect(Geometry.intersect(one, other)).toStrictEqual(other);
    });
    // -    one is NE of the other
    test('intersect Rectangle and Rectangel: one is NE of the other - no intersection', () => {
        const one: Rectangle = new Rectangle(50, 10, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toBe(undefined);
    });
    test('intersect Rectangle and Rectangel: one is NE of the other - intersection', () => {
        const one: Rectangle = new Rectangle(35, 15, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toStrictEqual(new Rectangle(35, 20, 5, 5));
    });
    test('intersect Rectangle and Rectangel: one is NE of the other - trivial case', () => {
        const one: Rectangle = new Rectangle(40, 10, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toStrictEqual(new Rectangle(40, 20, 0, 0));
    });
    // -    one is W of the other
    test('intersect Rectangle and Rectangel: one is W of the other - no intersection', () => {
        const one: Rectangle = new Rectangle(0, 15, 5, 5);
        const other: Rectangle = new Rectangle(10, 10, 10, 10);
        expect(Geometry.intersect(one, other)).toBe(undefined);
    });
    test('intersect Rectangle and Rectangel: one is W of the other - intersection', () => {
        const one: Rectangle = new Rectangle(8, 12, 5, 5);
        const other: Rectangle = new Rectangle(10, 10, 10, 10);
        expect(Geometry.intersect(one, other)).toStrictEqual(new Rectangle(10, 12, 3, 5));
    });
    test('intersect Rectangle and Rectangel: one is W of the other - trivial case', () => {
        const one: Rectangle = new Rectangle(5, 15, 5, 5);
        const other: Rectangle = new Rectangle(10, 10, 20, 20);
        expect(Geometry.intersect(one, other)).toStrictEqual(new Rectangle(10, 15, 0, 5));
    });
    test('intersect Rectangle and Rectangel: one is W of the other - one contains the other', () => {
        const one: Rectangle = new Rectangle(10, 10, 20, 10);
        const other: Rectangle = new Rectangle(10, 10, 10, 10);
        expect(Geometry.intersect(one, other)).toStrictEqual(other);
    });
    // -    one is inside the other
    test('intersect Rectangle and Rectangel: one is inside the other', () => {
        const one: Rectangle = new Rectangle(25, 25, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toStrictEqual(one);
    });
    test('intersect Rectangle and Rectangel: one is N of the other - match', () => {
        const one: Rectangle = new Rectangle(20, 20, 20, 20);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toStrictEqual(one);
    });
    // -    one is E of the other
    test('intersect Rectangle and Rectangel: one is E of the other - no intersection', () => {
        const one: Rectangle = new Rectangle(50, 25, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toBe(undefined);
    });
    test('intersect Rectangle and Rectangel: one is E of the other - intersection', () => {
        const one: Rectangle = new Rectangle(35, 25, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toStrictEqual(new Rectangle(35, 25, 5, 10));
    });
    test('intersect Rectangle and Rectangel: one is E of the other - trivial case', () => {
        const one: Rectangle = new Rectangle(40, 25, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toStrictEqual(new Rectangle(40, 25, 0, 10));
    });
    // -    one is SW of the other
    test('intersect Rectangle and Rectangel: one is SW of the other - no intersection', () => {
        const one: Rectangle = new Rectangle(10, 35, 5, 5);
        const other: Rectangle = new Rectangle(10, 10, 10, 10);
        expect(Geometry.intersect(one, other)).toBe(undefined);
    });
    test('intersect Rectangle and Rectangel: one is SW of the other - trivial case', () => {
        const one: Rectangle = new Rectangle(5, 20, 5, 5);
        const other: Rectangle = new Rectangle(10, 10, 10, 10);
        expect(Geometry.intersect(one, other)).toStrictEqual(new Rectangle(10, 20, 0, 0));
    });
    // -    one is S of the other
    test('intersect Rectangle and Rectangel: one is S of the other - no intersection', () => {
        const one: Rectangle = new Rectangle(25, 45, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toBe(undefined);
    });
    test('intersect Rectangle and Rectangel: one is S of the other - intersection', () => {
        const one: Rectangle = new Rectangle(25, 35, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toStrictEqual(new Rectangle(25, 35, 10, 5));
    });
    test('intersect Rectangle and Rectangel: one is s of the other - trivial case', () => {
        const one: Rectangle = new Rectangle(25, 40, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toStrictEqual(new Rectangle(25, 40, 10, 0));
    });
    // -    one is SE of the other
    test('intersect Rectangle and Rectangel: one is SE of the other - no intersection', () => {
        const one: Rectangle = new Rectangle(50, 50, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toBe(undefined);
    });
    test('intersect Rectangle and Rectangel: one is SE of the other - intersection', () => {
        const one: Rectangle = new Rectangle(35, 35, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toStrictEqual(new Rectangle(35, 35, 5, 5));
    });
    test('intersect Rectangle and Rectangel: one is NE of the other - trivial case', () => {
        const one: Rectangle = new Rectangle(40, 40, 10, 10);
        const other: Rectangle = new Rectangle(20, 20, 20, 20);
        expect(Geometry.intersect(one, other)).toStrictEqual(new Rectangle(40, 40, 0, 0));
    });

    // HalfPlane intersections
    // -    HorizontalTop
    test('intersect Rectangle and HalfPlane(HorizontalTop): Rectangle in the top area', () => {
        const rectangle: Rectangle = new Rectangle(0, 0, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(0, 20, Orientation.HorizontalTop);
        expect(Geometry.intersect(rectangle, halfPlane)).toBe(rectangle);
    });
    test('intersect Rectangle and HalfPlane(HorizontalTop): Rectangle in the bottom area', () => {
        const rectangle: Rectangle = new Rectangle(0, 30, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(0, 20, Orientation.HorizontalTop);
        expect(Geometry.intersect(rectangle, halfPlane)).toBe(undefined);
    });
    test('intersect Rectangle and HalfPlane(HorizontalTop): Rectangle actually intersects', () => {
        const rectangle: Rectangle = new Rectangle(0, 10, 20, 20);
        const halfPlane: HalfPlane = new HalfPlane(0, 20, Orientation.HorizontalTop);
        expect(Geometry.intersect(rectangle, halfPlane)).toStrictEqual(new Rectangle(0, 10, 20, 10));
    });
    test('intersect Rectangle and HalfPlane(HorizontalTop): Rectangle actually intersects - trivial case', () => {
        const rectangle: Rectangle = new Rectangle(0, 20, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(0, 20, Orientation.HorizontalTop);
        expect(Geometry.intersect(rectangle, halfPlane)).toStrictEqual(new Rectangle(0, 20, 10, 0));
    });
    // -    HorizontalBottom
    test('intersect Rectangle and HalfPlane(HorizontalBottom): Rectangle in the top area', () => {
        const rectangle: Rectangle = new Rectangle(0, 0, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(0, 20, Orientation.HorizontalBottom);
        expect(Geometry.intersect(rectangle, halfPlane)).toBe(undefined);
    });
    test('intersect Rectangle and HalfPlane(HorizontalBottom): Rectangle in the bottom area', () => {
        const rectangle: Rectangle = new Rectangle(0, 30, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(0, 20, Orientation.HorizontalBottom);
        expect(Geometry.intersect(rectangle, halfPlane)).toBe(rectangle);
    });
    test('intersect Rectangle and HalfPlane(HorizontalBottom): Rectangle actually intersects', () => {
        const rectangle: Rectangle = new Rectangle(0, 10, 20, 20);
        const halfPlane: HalfPlane = new HalfPlane(0, 20, Orientation.HorizontalBottom);
        expect(Geometry.intersect(rectangle, halfPlane)).toStrictEqual(new Rectangle(0, 20, 20, 10));
    });
    test('intersect Rectangle and HalfPlane(HorizontalBottom): Rectangle actually intersects - trivial case', () => {
        const rectangle: Rectangle = new Rectangle(0, 10, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(0, 20, Orientation.HorizontalBottom);
        expect(Geometry.intersect(rectangle, halfPlane)).toStrictEqual(new Rectangle(0, 20, 10, 0));
    });
    // -    VerticalLeft
    test('intersect Rectangle and HalfPlane(VerticalLeft): Rectangle in the left area', () => {
        const rectangle: Rectangle = new Rectangle(0, 0, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(20, 0, Orientation.VerticalLeft);
        expect(Geometry.intersect(rectangle, halfPlane)).toBe(rectangle);
    });
    test('intersect Rectangle and HalfPlane(VerticalLeft): Rectangle in the right area', () => {
        const rectangle: Rectangle = new Rectangle(30, 0, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(20, 0, Orientation.VerticalLeft);
        expect(Geometry.intersect(rectangle, halfPlane)).toBe(undefined);
    });
    test('intersect Rectangle and HalfPlane(VerticalLeft): Rectangle actually intersects', () => {
        const rectangle: Rectangle = new Rectangle(10, 0, 20, 20);
        const halfPlane: HalfPlane = new HalfPlane(20, 0, Orientation.VerticalLeft);
        expect(Geometry.intersect(rectangle, halfPlane)).toStrictEqual(new Rectangle(10, 0, 10, 20));
    });
    test('intersect Rectangle and HalfPlane(VerticalLeft): Rectangle actually intersects - trivial case', () => {
        const rectangle: Rectangle = new Rectangle(20, 0, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(20, 0, Orientation.VerticalLeft);
        expect(Geometry.intersect(rectangle, halfPlane)).toStrictEqual(new Rectangle(20, 0, 0, 10));
    });
    // -    VerticalRight
    test('intersect Rectangle and HalfPlane(VerticalRight): Rectangle in the left area', () => {
        const rectangle: Rectangle = new Rectangle(0, 0, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(20, 0, Orientation.VerticalRight);
        expect(Geometry.intersect(rectangle, halfPlane)).toBe(undefined);
    });
    test('intersect Rectangle and HalfPlane(VerticalRight): Rectangle in the bottom area', () => {
        const rectangle: Rectangle = new Rectangle(30, 0, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(20, 0, Orientation.VerticalRight);
        expect(Geometry.intersect(rectangle, halfPlane)).toBe(rectangle);
    });
    test('intersect Rectangle and HalfPlane(VerticalRight): Rectangle actually intersects', () => {
        const rectangle: Rectangle = new Rectangle(10, 0, 20, 20);
        const halfPlane: HalfPlane = new HalfPlane(20, 0, Orientation.VerticalRight);
        expect(Geometry.intersect(rectangle, halfPlane)).toStrictEqual(new Rectangle(20, 0, 10, 20));
    });
    test('intersect Rectangle and HalfPlane(VerticalRight): Rectangle actually intersects - trivial case', () => {
        const rectangle: Rectangle = new Rectangle(10, 0, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(20, 0, Orientation.VerticalRight);
        expect(Geometry.intersect(rectangle, halfPlane)).toStrictEqual(new Rectangle(20, 0, 0, 10));
    });

    // HalfPlane offsets
    // -    HorizontalTop
    test('HalfPlane(HorizontalTop) offsets Rectangle: Rectangle in the top area', () => {
        let rectangle: Rectangle = new Rectangle(0, 0, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(0, 20, Orientation.HorizontalTop);
        halfPlane.offset(rectangle);
        expect(rectangle.x).toStrictEqual(0);
        expect(rectangle.y).toStrictEqual(20);
    });
    test('HalfPlane(HorizontalTop) offsets Rectangle: Rectangle in the bottom area', () => {
        let rectangle: Rectangle = new Rectangle(0, 30, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(0, 20, Orientation.HorizontalTop);
        halfPlane.offset(rectangle);
        expect(rectangle.x).toStrictEqual(0);
        expect(rectangle.y).toStrictEqual(30);
    });
    test('HalfPlane(HorizontalTop) offsets Rectangle: Rectangle on the edge', () => {
        let rectangle: Rectangle = new Rectangle(0, 15, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(0, 20, Orientation.HorizontalTop);
        halfPlane.offset(rectangle);
        expect(rectangle.x).toStrictEqual(0);
        expect(rectangle.y).toStrictEqual(20);
    });
    // -    HorizontalBottom
    test('HalfPlane(HorizontalBottom) offsets Rectangle: Rectangle in the top area', () => {
        let rectangle: Rectangle = new Rectangle(0, 0, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(0, 20, Orientation.HorizontalBottom);
        halfPlane.offset(rectangle);
        expect(rectangle.x).toStrictEqual(0);
        expect(rectangle.y).toStrictEqual(0);
    });
    test('HalfPlane(HorizontalBottom) offsets Rectangle: Rectangle in the bottom area', () => {
        let rectangle: Rectangle = new Rectangle(0, 30, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(0, 20, Orientation.HorizontalBottom);
        halfPlane.offset(rectangle);
        expect(rectangle.x).toStrictEqual(0);
        expect(rectangle.y).toStrictEqual(20 - 10);
    });
    test('HalfPlane(HorizontalBottom) offsets Rectangle: Rectangle on the edge', () => {
        let rectangle: Rectangle = new Rectangle(0, 15, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(0, 20, Orientation.HorizontalBottom);
        halfPlane.offset(rectangle);
        expect(rectangle.x).toStrictEqual(0);
        expect(rectangle.y).toStrictEqual(20 - 10);
    });
    // -    VerticalLeft
    test('HalfPlane(VerticalLeft) offsets Rectangle: Rectangle in the left area', () => {
        let rectangle: Rectangle = new Rectangle(0, 0, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(20, 0, Orientation.VerticalLeft);
        halfPlane.offset(rectangle);
        expect(rectangle.x).toStrictEqual(20);
        expect(rectangle.y).toStrictEqual(0);
    });
    test('HalfPlane(VerticalLeft) offsets Rectangle: Rectangle in the right area', () => {
        let rectangle: Rectangle = new Rectangle(30, 0, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(20, 0, Orientation.VerticalLeft);
        halfPlane.offset(rectangle);
        expect(rectangle.x).toStrictEqual(30);
        expect(rectangle.y).toStrictEqual(0);
    });
    test('HalfPlane(VerticalLeft) offsets Rectangle: Rectangle on the edge', () => {
        let rectangle: Rectangle = new Rectangle(15, 0, 10, 10);
        const halfPlane: HalfPlane = new HalfPlane(20, 0, Orientation.VerticalLeft);
        halfPlane.offset(rectangle);
        expect(rectangle.x).toStrictEqual(20);
        expect(rectangle.y).toStrictEqual(0);
    });
});