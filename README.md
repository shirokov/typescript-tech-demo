This is an enhanced version of my [Typescript Canvas Test](https://gitlab.com/shirokov/typescript-canvas-test):
* split into a few source files and bundled via webpack
* application states stack implementation
* in the default mode balls are spawn by clicking the mouse
* balls bounce from each other
* game switches to a special state if the screen is too small
* the FPS counter is toggable with the 'F' key
* concepts of scene and hitbox boundary
* unit tests via Jest

Install deps:
`npm install typescript ts-loader webpack webpack-cli jest jest-cli ts-jest @types/jest --save-dev`

Build:
`npx webpack --config webpack.config.js`

Test:
`npx jest`

TODO:
* VSCode task for unit tests
* Release and Debug build profiles (Release also runs unit tests?)
* bottom panel that displays info
* make the bottom panel togglable
* collision sounds
* background music
* arbitrary trajectories of things - vectors
* switch trough backgrounds on keypress
* pause state
* display controls state - make it initial?
* backgrounds: solid color, tiled, generated
* optimize drawing - redraw only pieced that needed
* optimize PresenceGrid.move() - see the corresponding `TODO`s
* fix: things should offset each other on collision