import Boundary from "./boundary";
import { Geometry } from "./geometry";

// TODO: half plane should take a vector or a point + angle
//        and conventional orientation, say, left of the vector
export enum Orientation {
        HorizontalTop,
        HorizontalBottom,
        VerticalLeft,
        VerticalRight,
};

export default class HalfPlane implements Boundary {
    public x: number;
    public y: number;
    public orientation: Orientation;

    constructor(x: number, y: number, orientation: Orientation) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }

    contains(x: number, y: number): boolean {
        if (this.orientation == Orientation.HorizontalTop) {
            return y <= this.y;
        } else if (this.orientation == Orientation.HorizontalBottom) {
            return y >= this.y;
        } else if (this.orientation == Orientation.VerticalLeft) {
            return x <= this.x;
        } else if (this.orientation == Orientation.VerticalRight) {
            return x >= this.x;
        } else {
            throw new Error('unexpected orientation of half plane');
        }
    }

    intersection(boundary: Boundary): Boundary | undefined {
        return Geometry.intersect(this, boundary);
    }

    offset(boundary: Boundary): void {
        Geometry.offset(this, boundary);
    }
}