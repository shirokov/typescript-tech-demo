import Boundary from './boundary';
import HalfPlane, { Orientation } from './half_plane';
import Rectange from './rectangle';

export namespace Geometry {
    function intersect_Rectangle_Rectangle(one: Rectange, other: Rectange): Rectange | undefined {
        const left = one.x < other.x ? one : other;
        const right = one.x < other.x ? other : one;
        if (left.x + left.width < right.x) {
            return undefined;
        }

        const top = one.y < other.y ? one : other;
        const bottom = one.y < other.y ? other : one;
        if (top.y + top.height < bottom.y) {
            return undefined;
        }

        return new Rectange(right.x, bottom.y, Math.min(left.x + left.width, right.x + right.width) - right.x, Math.min(top.y + top.height, bottom.y + bottom.height) - bottom.y);
    }

    function instersect_Rectangle_HalfPlane(rectangle: Rectange, halfPlane: HalfPlane): Rectange | undefined {
        if (halfPlane.orientation == Orientation.HorizontalTop) {
            if (rectangle.y + rectangle.height <= halfPlane.y) {
                return rectangle;
            } else if (rectangle.y > halfPlane.y) {
                return undefined;
            } else {
                return new Rectange(rectangle.x, rectangle.y, rectangle.width, halfPlane.y - rectangle.y);
            }
        } else if (halfPlane.orientation == Orientation.HorizontalBottom) {
            if (rectangle.y >= halfPlane.y) {
                return rectangle;
            } else if (rectangle.y + rectangle.height < halfPlane.y) {
                return undefined;
            } else {
                return new Rectange(rectangle.x, halfPlane.y, rectangle.width, rectangle.y + rectangle.height - halfPlane.y);
            }
        } else if (halfPlane.orientation == Orientation.VerticalLeft) {
            if (rectangle.x + rectangle.width <= halfPlane.x) {
                return rectangle;
            } else if (rectangle.x > halfPlane.x) {
                return undefined;
            } else {
                return new Rectange(rectangle.x, rectangle.y, halfPlane.x - rectangle.x, rectangle.height);
            }
        } else if (halfPlane.orientation == Orientation.VerticalRight) {
            if (rectangle.x >= halfPlane.x) {
                return rectangle;
            } else if (rectangle.x + rectangle.width < halfPlane.x) {
                return undefined;
            } else {
                return new Rectange(halfPlane.x, rectangle.y, rectangle.x + rectangle.width - halfPlane.x, rectangle.height);
            }
        } else {
            throw new Error('unexpected orientation of half plane');
        }
    }

    function instersect_HalfPlane_HalfPlane(_: HalfPlane, __:HalfPlane): Boundary | undefined {
        // FIXME: implement
        throw new Error("intersect not implemented");
    }

    export function intersect(left: Boundary, right: Boundary): Boundary | undefined {
        if (left instanceof Rectange) {
            if (right instanceof Rectange) {
                return intersect_Rectangle_Rectangle(left, right);
            } else if (right instanceof HalfPlane) {
                return instersect_Rectangle_HalfPlane(left, right);
            }
        } else if (left instanceof HalfPlane) {
            if (right instanceof Rectange) {
                return instersect_Rectangle_HalfPlane(right, left);
            } else if (right instanceof HalfPlane) {
                return instersect_HalfPlane_HalfPlane(left, right);
            }
        }

        throw new Error("unhandled intersect");
    }

    function offset_Rectangle_Rectangle(_: Rectange, __: Rectange) {
        // FIXME: implement!
    }

    function offset_HalfPlane_Rectangle(halfPlane: HalfPlane, rectangle: Rectange) {
        switch (halfPlane.orientation) {
            case Orientation.HorizontalTop:
                if (rectangle.y < halfPlane.y) {
                    rectangle.y = halfPlane.y;
                }
                break;
            case Orientation.HorizontalBottom:
                if (rectangle.y + rectangle.height > halfPlane.y) {
                    rectangle.y = halfPlane.y - rectangle.height;
                }
                break;
            case Orientation.VerticalLeft:
                if (rectangle.x < halfPlane.x) {
                    rectangle.x = halfPlane.x;
                }
                break;
            case Orientation.VerticalRight:
                if (rectangle.x + rectangle.width > halfPlane.x) {
                    rectangle.x = halfPlane.x - rectangle.width;
                }
                break;
            default:
                throw new Error('unhandled orientation')
        }
    }

    export function offset(left: Boundary, right: Boundary): void {
        if (left instanceof Rectange) {
            if (right instanceof Rectange) {
                return offset_Rectangle_Rectangle(left, right);
            }
        } else if (left instanceof HalfPlane) {
            if (right instanceof Rectange) {
                return offset_HalfPlane_Rectangle(left, right);
            }
        }

        throw new Error("unhandled offset");
    }
}