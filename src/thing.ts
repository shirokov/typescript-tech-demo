import Boundary from './boundary';
import { Entity } from './presence_grid';
import Rectangle from './rectangle';
import Scene from './scene';
import * as consts from './consts';

export default class Thing implements Entity {
    private hitbox: Rectangle;
    private vX: number;
    private vY: number;

    // generated at https://colordesigner.io/gradient-generator
    private static readonly colors: string[] = ['#520abe',
                                                '#8b00b4',
                                                '#b500a6',
                                                '#d50095',
                                                '#ef0083',
                                                '#ff0070',
                                                '#ff005c',
                                                '#ff0048',
                                                '#ff4232',
                                                '#ff6713',
                                                '#ff8500',
                                                '#ffa100',
                                                '#ffbb00',
                                                '#fad300',
                                                '#e8ea00',
                                                '#d2ff00',];
    private color: number = 0;
    private colorDirection: number = 1;
    private timerTicks = 0;

    // [Entity]
    get boundary(): Boundary {
        return this.hitbox;
    }

    collide(entity: Entity, _: Boundary): void {
        // collide only with other Things
        if (!(entity instanceof Thing)) {
            return;
        }

        // confirm collision
        const thisMidX = this.hitbox.x + this.hitbox.width / 2;
        const thisMidY = this.hitbox.y + this.hitbox.height / 2;
        const thisRadius = this.hitbox.width / 2;
        const thatMidX = entity.hitbox.x + entity.hitbox.width / 2;
        const thatMidY = entity.hitbox.y + entity.hitbox.height / 2;
        const thatRadius = this.hitbox.width / 2;

        if (Math.sqrt(Math.pow(thisMidX - thatMidX, 2) + Math.pow(thisMidY - thatMidY, 2)) > thisRadius + thatRadius) {
            return;
        }

        // bounce
        const dX = thatMidX - thisMidX;
        if (dX * this.vX > 0) {
            this.vX *= -1;
        }

        const dY = thatMidY - thisMidY;
        if (dY * this.vY > 0) {
            this.vY *= -1;
        }
    }

    // [Thing]
    constructor(x: number, y: number, vX: number, vY: number) {
        if (Thing.colors.length < 2) {
            throw new Error("need at least two colors");
        }

        this.hitbox = new Rectangle(x, y, consts.TILE_SIZE, consts.TILE_SIZE);
        this.vX = vX;
        this.vY = vY;
    }

    tick(scene: Scene): void {
        this.timerTicks++;
        if (this.timerTicks === 10) {
            this.changeColor();
            this.timerTicks = 0;
        }

        let newHitbox = new Rectangle(this.hitbox.x + this.vX, this.hitbox.y + this.vY, this.hitbox.width, this.hitbox.height);
        this.bounce(newHitbox, scene);
        scene.move(this, newHitbox);
        this.hitbox = newHitbox;
    }

    private bounce(newHitbox: Rectangle, boundary: Boundary): void {
        const intersection = boundary.intersection(newHitbox);
        if (intersection) {
            if ((intersection.contains(newHitbox.x, newHitbox.y) && intersection.contains(newHitbox.x + newHitbox.width, newHitbox.y))
                    || (intersection.contains(newHitbox.x, newHitbox.y + newHitbox.height) && intersection.contains(newHitbox.x + newHitbox.width, newHitbox.y + newHitbox.height))) {
                this.vY *= -1;
            }
            if (intersection.contains(newHitbox.x, newHitbox.y) && intersection.contains(newHitbox.x, newHitbox.y + newHitbox.height)
                    || (intersection.contains(newHitbox.x + newHitbox.width, newHitbox.y) && intersection.contains(newHitbox.x + newHitbox.width, newHitbox.y + newHitbox.height))) {
                this.vX *= -1;
            }

            boundary.offset(newHitbox);
        }
    }

    private changeColor() {
        this.color += this.colorDirection;
        if (this.color < 0 ) {
            this.color = 1;
            this.colorDirection = 1;
        } else if (this.color === Thing.colors.length ) {
            this.color = Thing.colors.length - 1;
            this.colorDirection = -1;
        }
    }

    draw(context: CanvasRenderingContext2D): void {
        context.fillStyle = Thing.colors[this.color];
        const x = this.hitbox.x + this.hitbox.width / 2;
        const y = this.hitbox.y + this.hitbox.height / 2;
        context.beginPath();
        context.arc(x, y, consts.TILE_SIZE / 2, 0, 2 * Math.PI);
        context.fill();
    }
}