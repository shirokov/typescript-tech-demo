import Boundary from "./boundary";

export default class Overlay implements Boundary {
    private boundaries: Array<Boundary> = [];

    // [Overlay]
    constructor(...boundaries: (Boundary | undefined)[]) {
        for (let boundary of boundaries) {
            if (boundary !== undefined) {
               this.boundaries.push(boundary);
            }
        }
    }

    decay(): Boundary | undefined {
        if (this.boundaries.length == 0) {
            return undefined;
        } else if (this.boundaries.length == 1) {
            return this.boundaries[0];
        } else {
            return this;
        }
    }

    // [Boundary]
    contains(x: number, y: number): boolean {
        for (let boundary of this.boundaries) {
            if (boundary.contains(x, y)) {
                return true;
            }
        }

        return false;
    }

    intersection(boundary: Boundary): Boundary | undefined {
        let intersections: Array<Boundary> = [];

        for (let part of this.boundaries) {
            const intersection = boundary.intersection(part);
            if (intersection) {
                intersections.push(intersection);
            }
        }

        return intersections.length > 0
                ? new Overlay(...intersections)
                : undefined;
    }

    offset(boundary: Boundary): void {
        for (let part of this.boundaries) {
            part.offset(boundary);
        }
    }
}