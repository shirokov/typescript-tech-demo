import Environment from './environment';

export default interface State {
    activate(env: Environment): void;

    update(millis: number, env: Environment): void;
    // TODO: pass environment as a readonly param to `draw` once Typescript supports that
    draw(context: CanvasRenderingContext2D, readonlyEnv: Environment): void;

    deactivate(env: Environment): void;

    onResize(env: Environment): void;
    onClick(x: number, y: number): void;
}