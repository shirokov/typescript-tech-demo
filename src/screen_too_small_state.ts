import Environment from './environment';
import State from './state';

export default class ScreenTooSmallState implements State {
    private origTitle: string = "";
    private lastWidth: number = -1;
    private lastHeight: number = -1;

    activate(env: Environment) {
        this.origTitle = env.title;
        env.title = "SCREEN IS TOO SMALL! - "  + this.origTitle;

        this.lastWidth = env.width;
        this.lastHeight = env.height;

        env.enforceRedraw();
    }

    update(_: number, env: Environment): void {
        if (env.width != this.lastWidth
            || env.height != this.lastHeight)
        {
            env.enforceRedraw();

            this.lastWidth = env.width;
            this.lastHeight = env.height;
        }
    }

    draw(context: CanvasRenderingContext2D, readonlyEnv: Environment): void {
        if (readonlyEnv.forceRedraw) {
            context.clearRect(0, 0, readonlyEnv.width, readonlyEnv.height);
            context.lineWidth = 10;
            context.strokeStyle = '#ff0000'

            context.moveTo(0, 0);
            context.lineTo(readonlyEnv.width, readonlyEnv.height);

            context.moveTo(0, readonlyEnv.height);
            context.lineTo(readonlyEnv.width, 0);

            context.stroke();
        }
    }

    deactivate(env: Environment): void {
        env.title = this.origTitle;
    }

    onResize(_: Environment): void {
        // pass
    }

    onClick(_: number, __: number): void {
        // pass
    }
}