export const TILE_SIZE = 32;
export const SCREEN_WIDTH = 350;
export const SCREEN_HEIGHT = 350;
export const TICK_MILLIS = 10;
export const PRESENCE_GRID_CELL_SIZE = TILE_SIZE;