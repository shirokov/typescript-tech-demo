export default interface Boundary {
    contains(x: number, y: number): boolean;
    intersection(other: Boundary): Boundary | undefined;
    offset(other: Boundary): void;
}