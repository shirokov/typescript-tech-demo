import * as consts from './consts';
import Environment from './environment';
import FpsCounter from './fps_counter';
import GameplayState from './gameplay_state';
import ScreenTooSmallState from './screen_too_small_state';
import State from './state';

export default class App implements Environment {
    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;
    private lastTimeStamp = 0;
    private fps: FpsCounter;
    private showFps:boolean = true;
    private states: Array<State> = [];
    private screenTooSmallState: ScreenTooSmallState;
    private _forceRedraw: boolean = true;

    // [Environment]
    get title(): string {
        return document.title;
    }

    set title(newTitle: string) {
        document.title = newTitle;
    }

    get width(): number {
        return this.canvas.width;
    }

    get height(): number {
        return this.canvas.height;
    }

    get forceRedraw(): boolean {
        return this._forceRedraw;
    }
    enforceRedraw(): void {
        this._forceRedraw = true;
    }

    pushState(state: State) {
        let prevState = this.states.at(-1);
        prevState?.deactivate(this);
        state.activate(this);
        this.states.push(state);
    }

    popState(): void {
        let prevState = this.states.pop();
        prevState?.deactivate(this);
        this.states.at(-1)?.activate(this);
    }

    // [App]
    constructor() {
        this.canvas = <HTMLCanvasElement>document.getElementById("canvas");
        this.context = App.getContext(this.canvas);
        this.lastTimeStamp = performance.now();

        this.fps = new FpsCounter(0, 0, 110, 40);
        this.screenTooSmallState = new ScreenTooSmallState();
        this.pushState(new GameplayState());

        this.onResize();  // to sets canvas dimensions & check for screen size
    }

    private static getContext(canvas: HTMLCanvasElement): CanvasRenderingContext2D
    {
        let result = canvas.getContext("2d", { alpha: false });
        if (result instanceof CanvasRenderingContext2D) {
            return result;
        }
        throw new Error('failed to obtain context');
    }

    run(): void {
        window.addEventListener('error', (event) => this.onError(event))
        window.addEventListener('click', (event) => this.onClick(event))
        window.addEventListener('keydown', (event) => this.onKeyDown(event))
        window.addEventListener('keyup', (event) => this.onKeyUp(event))
        window.addEventListener('resize', (_) => this.onResize());

        window.requestAnimationFrame((now) => this.draw(now));
    }

    private getCurrentState(): State {
        let result = this.states.at(-1);
        if (!result) {
            throw new Error('states stack is empty');
        }
        return result!;
    }

    private onError(error: ErrorEvent): void {
        alert(error.message);
    }

    private onClick(event: MouseEvent): void {
        this.getCurrentState().onClick(event.x, event.y);
    }

    private onKeyDown(_: KeyboardEvent): void {
        // pass
    }

    private onKeyUp(event: KeyboardEvent): void {
        if (event.key === 'F') {
            this.showFps = !this.showFps;
            this.enforceRedraw();
        }
    }

    private onResize(): void {
        if (this.getCurrentState() instanceof ScreenTooSmallState) {
            if (window.innerWidth >= consts.SCREEN_WIDTH && window.innerHeight >= consts.SCREEN_HEIGHT) {
                this.popState();
            }
        }
        else {
            if (window.innerWidth < consts.SCREEN_WIDTH || window.innerHeight < consts.SCREEN_HEIGHT) {
                this.pushState(this.screenTooSmallState);
            }
        }

        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        this.getCurrentState().onResize(this);
    }

    private draw(now: number): void {
        const millis = now - this.lastTimeStamp;
        this.fps.update(millis);

        let state = this.getCurrentState();
        state.update(millis, this);

        state.draw(this.context, this);
        if (this.showFps) {
            this.fps.draw(this.context);
        }
        this._forceRedraw = false;

        window.requestAnimationFrame((now) => this.draw(now));
        this.lastTimeStamp = now;
    }
}