import Boundary from "./boundary";
import Rectangle from "./rectangle";

export interface Entity {
    get boundary(): Boundary;
    collide(entity: Entity, intersection: Boundary): void;
};

class CollidingPair {
    public one: Entity;
    public other: Entity;
    public intersection: Boundary;

    constructor(one: Entity, other: Entity, intersection: Boundary) {
        this.one = one;
        this.other = other;
        this.intersection = intersection;
    }

    toString(): string {
        return JSON.stringify(this);
    }
};

class SquareCell {
    private set: Set<Entity> = new Set<Entity>;

    add(entity: Entity): number {
        if(this.set.has(entity)) {
            throw new Error('adding entity that is already present');
        }

        this.set.add(entity);
        return this.set.size;
    }

    remove(entity: Entity): void {
        if (!this.set.delete(entity)) {
            throw new Error('removing entity that is not present');
        }
    }

    collide(collidingPairs: Map<string, CollidingPair>): void {
        if (this.set.size < 2) {
            return;
        }

        const arr = Array.from(this.set);
        for (let i = 0; i < arr.length; i++) {
            for (let j = i + 1; j < arr.length; j++) {
                const intersection = arr[i].boundary.intersection(arr[j].boundary);
                if (intersection) {
                    const collidingPair1 = new CollidingPair(arr[i], arr[j], intersection);
                    const collidingPair2 = new CollidingPair(arr[j], arr[i], intersection);
                    collidingPairs.set(collidingPair1.toString(), collidingPair1);
                    collidingPairs.set(collidingPair2.toString(), collidingPair2);
                }
            }
        }
    }
};

export default class PresenceGrid {
    private cellSize: number = 0;
    private grid: SquareCell[][] = new Array<Array<SquareCell>>;
    private collisions: Set<SquareCell> = new Set<SquareCell>;

    constructor(cellSize: number, width: number, height: number) {
        if (cellSize <= 0) {
            throw new Error("bad cell size");
        }

        this.cellSize = cellSize;
        this.resize(width, height);
    }

    resize(width: number, height: number): void {
        const oldGridWidth = this.grid.length;
        const oldGridHeight = this.grid.length > 0 ? this.grid[0].length : 0;
        const newGridWidth = Math.ceil(width / this.cellSize);
        const newGridHeight = Math.ceil(height / this.cellSize);

        // NOTE: we never shrink the grid

        for (let w = oldGridWidth; w < newGridWidth; w++) {
            this.grid[w] = new Array<SquareCell>;
            for (let h = 0; h < oldGridHeight; h++) {
                this.grid[w][h] = new SquareCell();
            }
        }

        for (let w = 0; w < newGridWidth; w++) {
            for (let h = oldGridHeight; h < newGridHeight; h++) {
                this.grid[w][h] = new SquareCell();
            }
        }
    }

    put(entity: Entity): void {
        this.applyToSpan(entity.boundary, (cell: SquareCell) => { this.addToCell(cell, entity); })
    }

    remove(entity: Entity): void {
        this.applyToSpan(entity.boundary, (cell: SquareCell) => { cell.remove(entity); });
    }

    move(entity: Entity, newBoundary: Boundary): void {
        // TODO: optimize - don't touch cells in the overlap of old and new boundaries
        this.applyToSpan(entity.boundary, (cell: SquareCell) => { cell.remove(entity); });
        this.applyToSpan(newBoundary, (cell: SquareCell) => { this.addToCell(cell, entity); })
    }

    resolveCollisions(): void {
        let collidingPairs = new Map<string, CollidingPair>;
        for (let cell of this.collisions) {
            cell.collide(collidingPairs);
        }

        for (let collidingPair of collidingPairs) {
            collidingPair[1].one.collide(collidingPair[1].other, collidingPair[1].intersection);
        }

        this.collisions.clear();
    }

    private addToCell(cell: SquareCell, entity: Entity): void {
        if (cell.add(entity) > 1) {
            this.collisions.add(cell);
        }
    }

    private applyToSpan(boundary: Boundary, callback: (cell: SquareCell) => void): void {
        if (boundary instanceof Rectangle) {
            this.applyToRectangleSpan(boundary, callback);
        } else {
            throw new Error('usupported boundary');
        }
    }

    private applyToRectangleSpan(rectangle: Rectangle, callback: (cell: SquareCell) => void): void {
        const left = Math.floor(rectangle.x / this.cellSize);
        const top = Math.floor(rectangle.y / this.cellSize);
        const right = Math.floor((rectangle.x + rectangle.width) / this.cellSize);
        const bottom = Math.floor((rectangle.y + rectangle.height) / this.cellSize);

        const gridWidth = this.grid.length;
        const gridHeight = this.grid.length > 0 ? this.grid[0].length : 0;

        if (right >= gridWidth || bottom >= gridHeight) {
            this.resize(rectangle.x + rectangle.width, rectangle.y + rectangle.height);
        }

        for (let w: number = left; w <= right; w++) {
            for (let h: number = top; h <= bottom; h++) {
                callback(this.grid[w][h]);
            }
        }
    }
}