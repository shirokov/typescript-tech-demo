import Boundary from "./boundary";
import { Geometry } from "./geometry";

export default class Rectangle implements Boundary {
    public x: number;
    public y: number;
    public width: number;
    public height: number;

    constructor(x: number = 0, y: number = 0, width: number = 0, height: number = 0) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    contains(x: number, y: number): boolean {
        return x >= this.x && x <= (this.x + this.width)
                && y >= this.y && y <= (this.y + this.height);
    }

    intersection(boundary: Boundary): Boundary | undefined {
        return Geometry.intersect(this, boundary);
    }

    offset(boundary: Boundary): void {
        Geometry.offset(this, boundary);
    }
}