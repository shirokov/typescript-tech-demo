import * as consts from './consts';
import Environment from "./environment";
import Scene from './scene';
import State from './state';

export default class GameplayState implements State {
    private tickerTimerMillis = 0;
    private scene: Scene = new Scene();

    activate(env: Environment): void {
        this.onResize(env)
    }

    update(millis: number, _: Environment): void {
        this.tickerTimerMillis += millis;
        if (this.tickerTimerMillis >= consts.TICK_MILLIS) {
            this.scene.tick();
            this.tickerTimerMillis = this.tickerTimerMillis % consts.TICK_MILLIS;
        }
    }

    draw(context: CanvasRenderingContext2D, readonlyEnv: Environment): void {
        // TODO: optimize drawing
        context.clearRect(0, 0, readonlyEnv.width, readonlyEnv.height);
        this.scene.draw(context);
    }

    deactivate(_: Environment): void {
        // pass
    }

    onResize(env: Environment) {
        this.scene.resize(env.width, env.height)
    }

    onClick(x: number, y: number): void {
        if (this.scene.contains(x, y))  {
            this.scene.onClick(x, y);
        }
    }
}