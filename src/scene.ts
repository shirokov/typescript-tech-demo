import Boundary from './boundary';
import Rectangle from './rectangle';
import HalfPlane, { Orientation } from './half_plane';
import Overlay from './overlay';
import PresenceGrid, { Entity } from './presence_grid';
import Thing from './thing';
import * as consts from './consts';

// TODO: all this Direction stuff is lame, change/remove!
enum Direction {
    UpLeft = 0,
    UpRight,
    DownRight,
    DownLeft,
    _Wraparound
  }

function directionToVY(d: Direction): number {
    if (d === Direction.UpLeft
        || d === Direction.UpRight) {
        return -2;
    }
    else {
        return 2;
    }
}

function directionToVX(d: Direction): number {
    if (d === Direction.UpRight
        || d === Direction.DownRight) {
        return 2;
    }
    else {
        return -2;
    }
}

function getEdges(rectangle: Rectangle): Overlay {
    return new Overlay(
        new HalfPlane(rectangle.x, rectangle.y, Orientation.HorizontalTop),
        new HalfPlane(rectangle.x, rectangle.y + rectangle.height, Orientation.HorizontalBottom),
        new HalfPlane(rectangle.x, rectangle.y, Orientation.VerticalLeft),
        new HalfPlane(rectangle.x + rectangle.width, rectangle.y, Orientation.VerticalRight),
    );
}

class RectangleWithEdges {
    public rectangle: Rectangle = new Rectangle();
    public edges: Overlay = getEdges(this.rectangle);

    resize(width: number, height: number): void {
        this.rectangle.width = width;
        this.rectangle.height = height;
        this.edges = getEdges(this.rectangle);
    }
}

export default class Scene implements Boundary {
    private things: Array<Thing> = [];
    private nextDirection: Direction = Direction.DownRight;
    private hitbox: RectangleWithEdges = new RectangleWithEdges();
    private presenceGrid: PresenceGrid = new PresenceGrid(consts.PRESENCE_GRID_CELL_SIZE, 0, 0);

    // [Boundary]
    // NOTE: `contains` checks if a point resides _inside_ the scene...
    contains(x: number, y: number): boolean {
        return this.hitbox.rectangle.contains(x, y);
    }

    // NOTE: ...but `intersection` works with the _outside_, in order to detect collisions...
    intersection(boundary: Boundary): Boundary | undefined {
        return this.hitbox.edges.intersection(boundary);
    }

    // NOTE: ...just as `offset` to properly bounce from the edges!
    offset(boundary: Boundary): void {
        this.hitbox.edges.offset(boundary);
    }

    // [Scene]
    tick() {
        for (let thing of this.things) {
            thing.tick(this);
        }

        this.presenceGrid.resolveCollisions();
    }

    draw(context: CanvasRenderingContext2D): void {
        for (let thing of this.things) {
            thing.draw(context);
        }
    }

    resize(width: number, height: number): void {
        this.hitbox.resize(width, height);
        this.presenceGrid.resize(width, height);
    }

    onClick(x: number, y: number): void {
        let newThing =  new Thing(x, y, directionToVX(this.nextDirection), directionToVY(this.nextDirection));
        this.presenceGrid.put(newThing);
        this.things.push(newThing);

        this.nextDirection += 1;
        if (this.nextDirection === Direction._Wraparound) {
            this.nextDirection = 0;
        }
    }

    move(entity: Entity, newBoundary: Boundary): void {
        this.presenceGrid.move(entity, newBoundary);
    }
}