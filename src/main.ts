import App from './app';

console.log = () => {};

try {
    let app = new App();
    app.run();
}
catch (e) {
    alert(e);
}