export default class FpsCounter {
    private readonly x: number;
    private readonly y: number;
    private readonly width: number;
    private readonly height: number;

    private frameCount = 0;
    private value = 0;
    private timerMillis = 0;

    constructor(x: number, y: number, width: number, height: number) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    update(millis: number): void {
        this.timerMillis += millis;
        if (this.timerMillis >= 1000) {
            this.value = Math.floor(1000 * this.frameCount / this.timerMillis);

            this.frameCount = 0;
            this.timerMillis = 0;
        }
    }

    draw(context: CanvasRenderingContext2D): void {
        this.frameCount++;

        context.fillStyle = 'white';
        context.fillRect(this.x, this.y, this.width, this.height);
        context.font = '25px Arial';
        context.fillStyle = 'black';
        context.fillText("FPS: " + (this.value > 0 ? this.value : "-"), 10, 30);
    }
}