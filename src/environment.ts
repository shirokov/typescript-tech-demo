import State from "./state";

export default interface Environment {
    get width(): number;
    get height(): number;
    get title(): string;
    set title(string);

    get forceRedraw(): boolean;
    enforceRedraw(): void;  // valid for one draw call only

    pushState(state: State): void;
    popState(): void;
}